//
//  NetworkingModule.swift
//  ChuckFacts
//
//  Created by AgentPro on 26/06/2020.
//  Copyright © 2020 InterZoneInc. All rights reserved.
//

import Foundation

// MARK: - Network Manager
class NetworkManager {
    
    // The session to use to download the data
    private let session: URLSession
    
    // Create the manager with a given session configuration
    init(_ sessionConfiguration: URLSessionConfiguration = .ephemeral) {
        session = URLSession(configuration: sessionConfiguration)
    }
    
    // A private function to fetch data from a URL
    private func fetchData(from url: URL, completion: @escaping (Data?) -> Void) {
        
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            if let error = error {
                print(error)
                completion(nil)
            } else if let response = response as? HTTPURLResponse {
                
                switch response.statusCode {
                case 200...299: completion(data)
                default: completion(nil)
                }
            }
        }
        
        dataTask.resume()
        
    }
    
    // A function to fetch random jokes
    func fetchRandomJokes(_ amount: Int = 1, limitExplicit: Bool, completion: @escaping (rootResponse?) -> Void) {
        
        // Create the URL Components
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host = "api.icndb.com"
        
        // Add the path
        urlComponents.path = "/jokes/random/\(amount)"
        
        // Add the optional exclude category if required, and add '?escape=javascript' in both cases to convert api behavior from HTML encoding special characters
        if limitExplicit {
            urlComponents.queryItems = [
                URLQueryItem(name: "exclude", value: "[explicit]"),
                URLQueryItem(name: "escape", value: "javascript")
            ]
        }
        else {
            urlComponents.queryItems = [URLQueryItem(name: "escape", value: "javascript")]
        }
        
        // Get the URL from the URL Components
        guard let randomJokesURL = urlComponents.url else {
            print("Unable to create URL")
            return completion(nil)
        }
        print(randomJokesURL)
        // Fetch the data
        fetchData(from: randomJokesURL) { data in
            if let data = data {
                
                do {
                    let value = try JSONDecoder().decode(rootResponse.self, from: data)
                    completion(value)
                } catch {
                    completion(nil)
                }
                
            } else {
                completion(nil)
            }
        }
    }
    
    
    // A function to fetch a single custom name joke
    func fetchCustomisedJoke(forFirstName firstName: String?, lastName: String?, limitExplicit: Bool, completion: @escaping (rootResponse?) -> Void) {
        
        guard let firstName = firstName else { return completion(nil)}
        guard let lastName = lastName else { return completion(nil)}
        
        
        // Create the URL Components
        var urlComponents = URLComponents()
        
        urlComponents.scheme = "https"
        urlComponents.host = "api.icndb.com"
        
        // Add the path
        urlComponents.path = "/jokes/random/1"
        
        // Add the first and last names
        // Add the optional exclude category if required, and add '?escape=javascript' in both cases to convert api behavior from HTML encoding special characters
        
        // FirstName must exist due to prior validation check. LastName may be left out. Two distinct queries are needed to avoid the API returning a blank space in lieu of the last name, leading to weird looking punctuation (i.e. "Dave 's first...".)
        if limitExplicit && lastName.isEmpty {
            urlComponents.queryItems = [
                URLQueryItem(name: "exclude", value: "[explicit]"),
                URLQueryItem(name: "escape", value: "javascript"),
                URLQueryItem(name: "firstName", value: firstName)
            ]
        } else if limitExplicit {
            urlComponents.queryItems = [
                URLQueryItem(name: "exclude", value: "[explicit]"),
                URLQueryItem(name: "escape", value: "javascript"),
                URLQueryItem(name: "firstName", value: firstName),
                URLQueryItem(name: "lastName", value: lastName)
            ]
        } else {
            urlComponents.queryItems = [
                URLQueryItem(name: "escape", value: "javascript"),
                URLQueryItem(name: "firstName", value: firstName),
                URLQueryItem(name: "lastName", value: lastName)
            ]
        }
        
        
        // Get the URL from the URL Components
        guard let randomJokesURL = urlComponents.url else {
            print("Unable to create URL")
            return completion(nil)
        }
        print(randomJokesURL)
        // Fetch the data
        fetchData(from: randomJokesURL) { data in
            if let data = data {
                
                do {
                    let value = try JSONDecoder().decode(rootResponse.self, from: data)
                    completion(value)
                } catch {
                    completion(nil)
                }
                
            } else {
                completion(nil)
            }
        }
    }
    
}

