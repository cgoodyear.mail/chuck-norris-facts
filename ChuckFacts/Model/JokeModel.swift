//
//  JokeModel.swift
//  ChuckFacts
//
//  Created by AgentPro on 26/06/2020.
//  Copyright © 2020 InterZoneInc. All rights reserved.
//

import Foundation


// MARK: - Root
struct rootResponse: Codable {
    let type: String
    let value: [Value]
}



// MARK: - Value
struct Value: Codable {
    let id: Int
    let joke: String
    let categories: [Category]
}

enum Category: String, Codable {
    case explicit = "explicit"
    case nerdy = "nerdy"
}


// MARK: - Joke Model

class JokeModel {
    
    private var response: rootResponse!
    
    private var currentJokes: [Value]!
    
    private var currentRandomJoke: String!
    
    // Default to true so you have to opt-in for rude jokes
    var limitExplicit: Bool = true
    
    private var networkLayer = NetworkManager()
    
    init() {
        response = rootResponse(type: "", value: [])
        currentJokes = []
        currentRandomJoke = ""
    }
    
    func refreshData(completion: @escaping () -> Void ) {
        
        networkLayer.fetchRandomJokes(50, limitExplicit: limitExplicit) { [unowned self] jokeResponse in
            
            if let response = jokeResponse {
                self.response = response
                self.currentJokes += response.value
                completion()
            }
        }
    }
    
    func getOneRandomJoke(completion: @escaping () -> Void ) {
        
        networkLayer.fetchRandomJokes(1, limitExplicit: limitExplicit) { [unowned self] jokeResponse in
            
            if let response = jokeResponse {
                self.currentRandomJoke = response.value[0].joke
                completion()
            }
        }
    }
    
    func getCustomisedJoke(forFirstName firstName: String?, lastName: String?, completion: @escaping () -> Void ) {
        
        networkLayer.fetchCustomisedJoke(forFirstName: firstName, lastName: lastName, limitExplicit: limitExplicit) { [unowned self] jokeResponse in
            
            if let response = jokeResponse {
                self.currentRandomJoke = response.value[0].joke
                completion()
            }
        }
    }
    
    func currentJoke() -> String {
        return currentRandomJoke
    }
}


extension JokeModel {
    
    var numberOfSections: Int {
        return 1
    }
    
    func numberOfRows(in section: Int) -> Int {
        guard section >= 0 && section < numberOfSections else { return 0 }
        //return response.value.count
        return currentJokes.count
    }
    
    func object(at indexPath: IndexPath) -> String? {
        
        guard indexPath.row >= 0 && indexPath.row < numberOfRows(in: indexPath.section) else { return nil}
       // return response.value[indexPath.row].joke
        return currentJokes[indexPath.row].joke
        
    }
    
}


