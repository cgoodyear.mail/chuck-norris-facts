//
//  String+ValidityType.swift
//  ChuckFacts
//
//  Created by AgentPro on 29/06/2020.
//  Copyright © 2020 InterZoneInc. All rights reserved.
//

import Foundation

extension String {
    
    enum ValidityType {
        case name
        // Allows for other cases, maybe search terms for specific Chuck jokes etc - Just add a ValidityType case and Regex case.
    }
    
    enum Regex: String {
        case name = "^\\w{3,18}$" // Length be 18 characters max and 3 characters minimum - any additional complexity required can be added to the regex here
    }
    
    func isValid(_ validityType: ValidityType) -> Bool {
        let format = "SELF MATCHES %@"
        var regex = ""
        let trimmedString = self.trimmingCharacters(in: .whitespaces)
        
        switch validityType {
        case .name: regex = Regex.name.rawValue
        }
        return NSPredicate(format: format, regex).evaluate(with: trimmedString)
    }
}
