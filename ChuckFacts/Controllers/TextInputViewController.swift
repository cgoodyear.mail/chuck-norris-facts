//
//  TextInputViewController.swift
//  ChuckFacts
//
//  Created by AgentPro on 26/06/2020.
//  Copyright © 2020 InterZoneInc. All rights reserved.
//

import UIKit

class TextInputViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    fileprivate var model: JokeModel!
    
    fileprivate var alertJoke: String = "Default Test"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model = JokeModel()
        
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        searchButton.isEnabled = false
        
    }
    
    @IBAction func showCustomButtonTapped(_ sender: Any) {
        
        // Validate the firstName field - I've decided to allow no last name so you can just enter a single name and still get a joke
        if (firstNameTextField.text?.isValid(.name)) ?? false {
            showJoke()
        } else {
            showInvalidTextEntryAlert()
        }
    }
    
    fileprivate func showJoke() {
        
        let child = SpinnerViewController()
        
        // add the spinner view controller
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
        
        
        let firstName = firstNameTextField.text
        let lastName = lastNameTextField.text
        
        
        //fetch a new joke
        model.getCustomisedJoke(forFirstName: firstName, lastName: lastName) {
            DispatchQueue.main.async {
                
                self.alertJoke = self.model.currentJoke()
                
                // create the alert
                let alert = UIAlertController(title: self.alertJoke, message: "", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
                
                // then remove the spinner view controller
                child.willMove(toParent: nil)
                child.view.removeFromSuperview()
                child.removeFromParent()
            }
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //textFields are tagged to allow the return key to flow UI from the first to last name without closing and reopening the keyboard for better UX
        
        let nextTag = textField.tag + 1
        
        if let nextResponder = textField.superview?.viewWithTag(nextTag) {
            nextResponder.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            searchButton.isEnabled = true
        }
        
        return true
    }
    
    func showInvalidTextEntryAlert() {
        // create the alert
        let alert = UIAlertController(title: "Invalid Name Entered", message: "Please enter a first name with 3 to 18 characters.", preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    }
}
