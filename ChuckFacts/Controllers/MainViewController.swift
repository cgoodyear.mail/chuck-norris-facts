//
//  MainViewController.swift
//  ChuckFacts
//
//  Created by AgentPro on 26/06/2020.
//  Copyright © 2020 InterZoneInc. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    
    @IBOutlet weak var nonExplicitLabel: UILabel!
    @IBOutlet weak var explicitSwitch: UISwitch!
    @IBOutlet weak var epicBackgroundImage: UIImageView!
    
    
    
    
    fileprivate var model: JokeModel!
    
    fileprivate var alertJoke: String = "Default Test"
    
    fileprivate func setEpicBackgroundAnimation() {
        epicBackgroundImage.animationImages =
            [UIImage(named: "1.jpeg")!,
             UIImage(named: "2.jpeg")!,
             UIImage(named: "3.jpeg")!,
             UIImage(named: "4.jpeg")!
        ]
        
        epicBackgroundImage.animationDuration = 1.3
        epicBackgroundImage.animationRepeatCount = 0
        epicBackgroundImage.startAnimating()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        model = JokeModel()
        
        //https://developer.apple.com/forums/thread/132035 re: "invalid mode 'kCFRunLoopCommonModes'" log noise on switch use.
        explicitSwitch.addTarget(self, action: #selector(toggleExplicit), for: .valueChanged)
        
        setEpicBackgroundAnimation()
        
    }
    
    
    fileprivate func showJoke() {
        
        let child = SpinnerViewController()
        
        // add the spinner view controller
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
        
        //fetch a new joke
        model.getOneRandomJoke {
            DispatchQueue.main.async {
                
                self.alertJoke = self.model.currentJoke()
                
                // create the alert
                let alert = UIAlertController(title: self.alertJoke, message: "", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
                
                
                // then remove the spinner view controller
                child.willMove(toParent: nil)
                child.view.removeFromSuperview()
                child.removeFromParent()
            }
        }
    }
    
    @IBAction func showRandomButtonTapped(_ sender: Any) {
        showJoke()
    }
    
    
    @objc func toggleExplicit() {
        model.limitExplicit.toggle()
    }
}

