//
//  NeverEndingTableViewController.swift
//  ChuckFacts
//
//  Created by AgentPro on 26/06/2020.
//  Copyright © 2020 InterZoneInc. All rights reserved.
//

import UIKit

class NeverEndingTableViewController: UITableViewController {
    
    var position: Int = 0
    
    // The data model
    fileprivate var model: JokeModel!
    
    // When the view appears, we update with random jokes
    override func viewDidLoad() {
        super.viewDidLoad()
        model = JokeModel()
        update()
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        
    }
    
    // When we update we fetch x random images then we reload the TableView
    func update() {
        
        let child = SpinnerViewController()
        
        // add the spinner view controller
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)

        model.refreshData {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                // then remove the spinner view controller
                child.willMove(toParent: nil)
                child.view.removeFromSuperview()
                child.removeFromParent()
            }
        }
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return model.numberOfSections
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numberOfRows(in: section)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CustomTableViewCell.reuseIdentifier, for: indexPath) as? CustomTableViewCell else { fatalError() }
        
        // Configure the cell...
        guard let joke = model.object(at: indexPath) else { return cell }
        cell.configure(cellPosition: indexPath.row, withJoke: joke)
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = model.numberOfRows(in: indexPath.section) - 1
        if indexPath.row == lastElement {
            update()
        }
    }
    
}
