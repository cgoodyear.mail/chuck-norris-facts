//
//  SpinnerViewController.swift
//  ChuckFacts
//
//  Created by AgentPro on 28/06/2020.
//  Copyright © 2020 InterZoneInc. All rights reserved.
//

import UIKit


// Activity notification had been added to all network requests for improved UX
class SpinnerViewController: UIViewController {
    
    
    var spinner = UIActivityIndicatorView(style: .whiteLarge)

    override func loadView() {
        view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.7)

        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.startAnimating()
        view.addSubview(spinner)

        spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
}
