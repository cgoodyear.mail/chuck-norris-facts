//
//  CustomTableViewCell.swift
//  ChuckFacts
//
//  Created by AgentPro on 26/06/2020.
//  Copyright © 2020 InterZoneInc. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    static public let reuseIdentifier = "custom_cell"
    
    @IBOutlet weak var jokeLabel: UILabel!
    
    func configure(cellPosition: Int, withJoke joke: String) {
        jokeLabel.text = joke
        
        // Alternate the UI colours for 90's fun - but actually turned out more readable.
        if cellPosition % 2 == 0 {
            self.backgroundColor = .red
            self.jokeLabel.textColor = .white
        } else {
            self.backgroundColor = .white
            self.jokeLabel.textColor = .black
        }
        
    }
    
}
